﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Configuration;
using CrearXml;
using SBODI_Server;

namespace webServiceEjemplo
{
    public class Sap
    {
        public static string SESSIONID;
        public static string server;
        public static string LicenseServer;
        public static string DatabaseType;
        public static string DatabaseName;
        public static string SAPUsername;
        public static string DBUsername;
        public static string DBPassword;
        public static string SAPUsernamePassword;
        public static string Lenguaje;

        public Sap() {
            server = ConfigurationManager.AppSettings["server"];
            LicenseServer = ConfigurationManager.AppSettings["LicenseServer"];
            DatabaseType = ConfigurationManager.AppSettings["DatabaseType"];
            DatabaseName = ConfigurationManager.AppSettings["DatabaseName"];
            SAPUsername = ConfigurationManager.AppSettings["SAPUsername"];
            SAPUsernamePassword = ConfigurationManager.AppSettings["SAPUsernamePassword"];
            DBUsername = ConfigurationManager.AppSettings["DBUsername"];
            DBPassword = ConfigurationManager.AppSettings["DBPassword"];
            Lenguaje = ConfigurationManager.AppSettings["Lenguaje"];
        }

        public static string ConectarSAP()
        {
            new Sap();
            DocumentoXml documento = new DocumentoXml();
            using (documento)
            {
                using (Nodo envelope = documento.addNodo("env", "Envelope", null, "http://schemas.xmlsoap.org/soap/envelope/"))
                {
                    using (Nodo body = envelope.addNodo("env", "Body", null))
                    {
                        using (Nodo Login = body.addNodo("dis", "Login", null, "http://www.sap.com/SBO/DIS"))
                        {
                            Login.addNodo(null, "DatabaseServer", server, null);
                            Login.addNodo(null, "DatabaseName", DatabaseName, null);
                            Login.addNodo(null, "DatabaseType", DatabaseType, null);
                            Login.addNodo(null, "DatabaseUsername", DBUsername, null);
                            Login.addNodo(null, "DatabasePassword", DBPassword, null);
                            Login.addNodo(null, "CompanyUsername", SAPUsername, null);
                            Login.addNodo(null, "CompanyPassword", SAPUsernamePassword, null);
                            Login.addNodo(null, "Language", Lenguaje, null);
                            Login.addNodo(null, "LicenseServer", LicenseServer, null);
                        }
                    }
                }
            }
            //documento.crearDocumento().Save("C:\\Users\\Descon2\\source\\repos\\webServiceEjemplo\\webServiceEjemplo\\Login.xml");
            string xmlLogin = documento.crearDocumento().OuterXml;
            Node pDISnode = new Node();
            string sSOAPans = pDISnode.Interact(xmlLogin);
            XmlDocument respuesta = new XmlDocument();
            respuesta.LoadXml(sSOAPans);

            //respuesta.Save("C:\\Users\\Descon2\\source\\repos\\webServiceEjemplo\\webServiceEjemplo\\Respuesta.xml");
            sSOAPans = respuesta.ChildNodes[1].ChildNodes[0].InnerText;

            return sSOAPans;

        }
        public static  XmlDocument login() {
            new Sap();
            DocumentoXml documento = new DocumentoXml();
            using (documento)
            {
                using (Nodo envelope = documento.addNodo("env", "Envelope", null, "http://schemas.xmlsoap.org/soap/envelope/"))
                {
                    using (Nodo body = envelope.addNodo("env", "Body", null))
                    {
                        using (Nodo Login = body.addNodo("dis", "Login", null, "http://www.sap.com/SBO/DIS"))
                        {
                            Login.addNodo(null, "DatabaseServer", server, null);
                            Login.addNodo(null, "DatabaseName", DatabaseName, null);
                            Login.addNodo(null, "DatabaseType", DatabaseType, null);
                            Login.addNodo(null, "DatabaseUsername", DBUsername, null);
                            Login.addNodo(null, "DatabasePassword", DBPassword, null);
                            Login.addNodo(null, "CompanyUsername", SAPUsername, null);
                            Login.addNodo(null, "CompanyPassword", SAPUsernamePassword, null);
                            Login.addNodo(null, "Language", Lenguaje, null);
                            Login.addNodo(null, "LicenseServer", LicenseServer, null);
                        }
                    }
                }
            }
            //documento.crearDocumento().Save("C:\\Users\\Descon2\\source\\repos\\webServiceEjemplo\\webServiceEjemplo\\Login.xml");
            string xmlLogin = documento.crearDocumento().OuterXml;
            Node pDISnode = new Node();
            string sSOAPans = pDISnode.Interact(xmlLogin);
            XmlDocument respuesta = new XmlDocument();
            respuesta.LoadXml(sSOAPans);

           //respuesta.Save("C:\\Users\\Descon2\\source\\repos\\webServiceEjemplo\\webServiceEjemplo\\Respuesta.xml");
            sSOAPans = respuesta.ChildNodes[1].ChildNodes[0].InnerText;
            SESSIONID = respuesta.ChildNodes[1].InnerText;
           
            return respuesta;
        }

        public static string Logout() {
         
            DocumentoXml documento = new DocumentoXml();
            using (documento)
            {
                using (Nodo envelope = documento.addNodo("env", "Envelope", null, "http://schemas.xmlsoap.org/soap/envelope/"))
                {
                    using (Nodo Header = envelope.addNodo("env","Header",null))
                    {
                        Header.addNodo("SessionID",Sap.SESSIONID);
                    }
                    using (Nodo body = envelope.addNodo("env", "Body", null))
                    {
                        Nodo Logout = body.addNodo("dis", "Logout", null, "http://www.sap.com/SBO/DIS");
                    }
                }
            }
           // documento.crearDocumento().Save("C:\\Users\\Descon2\\source\\repos\\webServiceEjemplo\\webServiceEjemplo\\Login.xml");
            string xmlLogin = documento.crearDocumento().OuterXml;
            Node pDISnode = new Node();
            string sSOAPans = pDISnode.Interact(xmlLogin);
            XmlDocument respuesta = new XmlDocument();
            respuesta.LoadXml(sSOAPans);

            return sSOAPans;
        }
    }
}