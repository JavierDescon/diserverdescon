﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using CrearXml;
using System.Xml;
using SBODI_Server;
using SAPbobsCOM;
using SAPbouiCOM;

namespace webServiceEjemplo
{
    /// <summary>
    /// Descripción breve de WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld(string nombre)
        {

            //string nombre = (from row in XDocument.Parse(xml.OuterXml).Descendants("nombre") select row.Value.ToUpper()).First();
            return "Funciona "+ nombre;
        }

        [WebMethod]
        public string AddOrder(string xml)
        {
            try
            {
                xml = xml.Replace("< ", "<");
                XmlDocument documento = new XmlDocument();
                documento.LoadXml(xml);
            
                Sap.login();
                documento.ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].InnerText = Sap.SESSIONID;
                string request = documento.OuterXml;
                Node pDISnode = new Node();
                string sResult = pDISnode.Interact(request);
                Sap.Logout();
                return sResult;
            }
            catch (Exception e)
            {
                return e.Message;
            }
           
            
        }
        [WebMethod]
        public XmlDocument PruebaConexion(){
            return Sap.login();
        }
        

    }
}
